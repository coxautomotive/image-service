var seneca = require('seneca')();

seneca.add({role: 'image', cmd: 'getById'}, function(msg, respond) {
  var image = {
    url: 'http://foo.com/book.jpg'
  }

  respond(null, image);
});

seneca.listen();

seneca.ready(function() {
  console.log('image-service ready!');
});
